import { API_URL, WEATHER_API_URL } from "@utils/constant";
import { isBrowser } from "@utils/utils";
import Strapi from "strapi-sdk-js";

const strapi = new Strapi(API_URL);

strapi.axios.interceptors.request.use((config) => {
    if (config.url && !config.url.includes(WEATHER_API_URL)) {
        config.withCredentials = true;
    }
    if (isBrowser && localStorage && localStorage.getItem("jwt")) {
        config.headers["Authorization"] =
            "Bearer " + localStorage.getItem("jwt");
    }
    return config;
});

export default strapi;
