import { API_URL, WEATHER_API_URL } from "@utils/constant";
import axios from "axios";

export const find = async (name: string, params?: object) => {
    const url = API_URL + name;
    try {
        const response = await axios.get(url, {
            params,
        });
        return response.data;
    } catch (error) {
        console.log(error);
    }
};

export const create = async (name: string, params?: object) => {
    const url = API_URL + name;
    try {
        const { data } = await axios.post(url, {
            ...params,
        });
        return data;
    } catch (error) {
        console.log(error);
    }
};

export const update = async (name: string, params?: object) => {
    const url = API_URL + name;
    try {
        const { data } = await axios.put(url, {
            ...params,
        });
        return data;
    } catch (error) {
        console.log(error);
    }
};

export const deleted = async (name: string) => {
    const url = API_URL + name;
    try {
        const { data } = await axios.delete(url);
        return data;
    } catch (error) {
        console.log(error);
    }
};

export const getWeather = async () => {
    try {
        const { data } = await axios.request({
            method: "GET",
            url: WEATHER_API_URL,
            params: { q: "Ho Chi Minh" },
        });
        return data;
    } catch (error) {
        console.log(error);
    }
};

const Request = {
    find,
    create,
    update,
    deleted,
    getWeather,
};

export default Request;
