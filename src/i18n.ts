import { isBrowser } from "@utils/utils";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import tsEN from "../public/locales/en/comon.json";
import tsKO from "../public/locales/ko/comon.json";
import tsVN from "../public/locales/vn/comon.json";

const resources = {
    en: { translation: tsEN },
    ko: { translation: tsKO },
    vn: { translation: tsVN },
};

i18n.use(initReactI18next).init({
    resources,
    lng: (isBrowser && localStorage && localStorage.getItem("lang")) || "ko",
    keySeparator: false,
    interpolation: {
        escapeValue: false,
    },
});

export default i18n;
