import IconBar from "@images/IconBar.svg";

const MenuBar = () => {
    return (
        <div>
            <button className="fixed right-0 transform -translate-y-1/2 top-1/2 no-decorate">
                <IconBar />
            </button>
        </div>
    );
};

export default MenuBar;
