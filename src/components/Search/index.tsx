import { useState } from "react";
import { Modal, Input } from "antd";
import IconBack from "@images/back.svg";
import { useTranslation } from "react-i18next";

const Search = () => {
    const { t } = useTranslation();
    const [visible, setVisible] = useState(false);
    const [search, setSearch] = useState("");
    const handleRemoveSearch = () => {
        setSearch("");
    };
    const toggleModal = () => {
        setVisible(!visible);
    };

    return (
        <div>
            <input
                type="text"
                placeholder={t("검색어를 입력해 주세요.")}
                className="text-sm bg-greyLight2 h-12 px-4 w-full rounded-6 tracking-secondary mt-4"
                onClick={() => setVisible(true)}
            />
            <Modal
                centered
                visible={visible}
                onOk={toggleModal}
                onCancel={toggleModal}
                width="100%"
                bodyStyle={{ height: "100vh" }}
                footer=""
                wrapClassName="search-modal"
                closable={false}
            >
                <div className="flex items-center space-x-4">
                    <button className="w-6 no-decorate" onClick={toggleModal}>
                        <IconBack />
                    </button>
                    <Input
                        placeholder={t("검색어를 입력해 주세요.")}
                        className="text-sm bg-greyLight2 h-12 px-2 w-full rounded tracking-secondary search-input no-decorate"
                        allowClear
                        onChange={(e) => {
                            setSearch(e.target.value);
                        }}
                        value={search}
                    />
                    {search && (
                        <button
                            className="text-slm text-primary"
                            onClick={handleRemoveSearch}
                        >
                            {t("Cancel")}
                        </button>
                    )}
                </div>
            </Modal>
        </div>
    );
};

export default Search;
