import { useTranslation } from "react-i18next";
import Icon from "@images/iwwa_option-horizontal.svg";

const MainNews = () => {
    const { t } = useTranslation();
    const data = [
        {
            id: 1,
            title: "뉴스",
            icon: "/images/news.svg",
            count: "125",
        },
        {
            id: 2,
            title: "매거진",
            icon: "/images/magazines.svg",
            count: "125",
        },
        {
            id: 3,
            title: "이벤트",
            icon: "/images/events.svg",
            count: "125",
        },
        {
            id: 4,
            title: "라이프",
            icon: "/images/advertisers.svg",
            count: "125",
        },
    ];
    return (
        <section className="py-4">
            <h2 className="text-lg font-bold tracking-wide">
                {t("이번 호 주요 뉴스")}
            </h2>
            <div className="mt-4">
                {data.map((x) => (
                    <div className="flex space-x-4 py-3" key={x.id}>
                        <img
                            src="/images/Rectangle 9.png"
                            className="w-20 h-20 bg-blue-500 flex-none rounded-10"
                            alt=""
                        />
                        <div className="flex flex-col justify-between">
                            <h5 className="text-sm">
                                "윤석열, 조국 수사 문 대통령 구하려 시작했다"
                            </h5>
                            <div className="flex justify-between">
                                <p className="text-xs">21.Mar.2021</p>
                                <a href="#">
                                    <Icon />
                                </a>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default MainNews;
