import { useTranslation } from "react-i18next";
import styles from "./styles.module.scss";

const RealtimeNews = () => {
    const { t } = useTranslation();
    const data = [
        {
            id: 1,
            title: "뉴스",
            icon: "/images/news.svg",
            count: "125",
        },
        {
            id: 2,
            title: "매거진",
            icon: "/images/magazines.svg",
            count: "125",
        },
        {
            id: 3,
            title: "이벤트",
            icon: "/images/events.svg",
            count: "125",
        },
        {
            id: 4,
            title: "라이프",
            icon: "/images/advertisers.svg",
            count: "125",
        },
    ];
    return (
        <section className="py-4">
            <h2 className="text-lg font-bold tracking-wide">
                {t("나우 온 신짜오 베트남")}
            </h2>
            <div
                className="flex mt-4 px-2 py-3 text-greyDark items-center rounded-10"
                style={{ background: "#DDFFF9" }}
            >
                <img src="/images/news2.svg" alt="news" />
                <p className="text-sm ml-4 mr-6">
                    베트남 서북부, 추수기 논길여행 5대 명소
                </p>
                <p className="ml-auto text-xs whitespace-nowrap	">
                    April 15,
                    <br />
                    2021
                </p>
            </div>
        </section>
    );
};

export default RealtimeNews;
