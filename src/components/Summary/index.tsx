import { useTranslation } from "react-i18next";
import styles from "./styles.module.scss";

const Summary = () => {
    const { t } = useTranslation();
    const data = [
        {
            id: 1,
            title: "뉴스",
            icon: "/images/news.svg",
            count: "125",
        },
        {
            id: 2,
            title: "매거진",
            icon: "/images/magazines.svg",
            count: "125",
        },
        {
            id: 3,
            title: "이벤트",
            icon: "/images/events.svg",
            count: "125",
        },
        {
            id: 4,
            title: "라이프",
            icon: "/images/advertisers.svg",
            count: "125",
        },
    ];
    return (
        <section className="py-4">
            <h2 className="text-lg font-bold tracking-wide">
                {t("나우 온 신짜오 베트남")}
            </h2>
            <div className="flex space-x-4 w-full overflow-hidden mt-3 pt-1">
                {data.map((x) => (
                    <div className="text-center" key={x.id}>
                        <img
                            className={`object-contain w-74 h-74 rounded-10 ${styles.realtimeItem}`}
                            src={x.icon}
                            alt={x.title}
                        />
                        <h4 className="text-primary font-semibold mt-2">
                            {t(x.title)}
                        </h4>
                        <p className="text-greyDark text-xs">({x.count})</p>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default Summary;
