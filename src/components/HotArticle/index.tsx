import { WEATHER_ICON_DEFAULT } from "@utils/constant";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import styles from "./styles.module.scss";

const HotArticle = () => {
    const { t } = useTranslation();
    let articles = [];
    for (let i = 0; i < 3; i++) {
        articles.push({ id: i });
    }
    return (
        <section id="hot-articles">
            <div className="flex justify-between items-center">
                <h2 className="text-lg font-bold tracking-wide">
                    {t("핫 헤드라인")}
                </h2>
                <p className="flex items-center">
                    <img src={WEATHER_ICON_DEFAULT} alt="weather" />
                    <span className="text-sm ml-1 font-semibold mb-2">
                        {t("28°C")}
                    </span>
                </p>
            </div>
            <div className="mt-4 space-x-4 overflow-x-scroll w-full flex width-plus-16">
                {articles.map((x) => (
                    <div
                        key={x.id}
                        className={`${styles.hotArticleItem} rounded-10 relative`}
                        style={{
                            background: `linear-gradient(180deg, rgba(0, 0, 0, 0) 51.31%, rgba(0, 0, 0, 0.7) 100%)`,
                        }}
                    >
                        <div className="text-white absolute bottom-0 left-0 w-full px-4 py-6 font-semibold text-xs">
                            <p>Apr 11, 2021</p>
                            <p className="mb-4 mt-3">...</p>
                            <p>
                                서민 "오세훈, 여론조사 좀 잘나온다고 눈에 뵈는게
                                없나" (feat 진중권)
                            </p>
                        </div>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default HotArticle;
