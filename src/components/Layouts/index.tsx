import Head from "next/head";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import MenuBar from "@components/MenuBar";
import Search from "@components/Search";
import Calendar from "@images/calendar.svg";

type LayoutProps = {
    title: string;
};

const Layout: React.FC<LayoutProps> = ({ children, title }) => {
    const { t } = useTranslation();
    const router = useRouter();
    const isHomePage = router.pathname === "/";

    return (
        <div className="mx-auto bg-white max-w-3xl px-4">
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=0"
                />
                <link
                    href="https://fonts.googleapis.com/css?family=Mulish"
                    rel="stylesheet"
                ></link>
                <title>{title}</title>
            </Head>
            {isHomePage && (
                <h1>
                    <span className="text-base">{t("Xin Chao")}</span>
                    <br />
                    <span className="font-bold text-xl">{t("Viet Nam")}</span>
                </h1>
            )}
            <MenuBar />
            <Search />
            <div className="bg-primary h-9 rounded-6 flex items-center text-white px-4 mt-2">
                <Calendar />
                <p className="text-sm font-bold ml-1">{t("Comming Soon")}</p>
                <p className="ml-auto">15.Mar 2021</p>
            </div>
            {children}
        </div>
    );
};

export default Layout;
