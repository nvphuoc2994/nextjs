import { atom } from "recoil";

export const statusPopupState = atom({
    key: "statusPopupState",
    default: false,
});
