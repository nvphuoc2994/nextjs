export const API_URL = "https://chaovietnam.abits.vn/";
export const WEATHER_API_KEY = "53c55a32c415485482593011211504";
export const WEATHER_ICON_DEFAULT = "/images/weather.svg";
export const WEATHER_API_URL =
    "//api.weatherapi.com/v1/current.json?key=" + WEATHER_API_KEY;
export const IMAGE_DEFAULT = "https://via.placeholder.com/400?text=ChaoVietNam";
export const DIVISION_NEW = 1;
export const DIVISION_MAGAZINE = 2;
export const SUBDIV_TIMELINE = 4;
export const SUBDIV_PUBLIC_NEWS = 3;
export const SUBDIV_MAGAZINE_TABS = [
    { id: 6, name: "Column", data: [] },
    { id: 7, name: "VN Info", data: [] },
    { id: 10, name: "People", data: [] },
];

export const DISPLAY_MENU = {
    MAIN_HEADLINE_NEWS: "MAIN_HEADLINE_NEWS",
    MAIN_TOP_NEWS: "MAIN_TOP_NEWS",
    MAGAZINE: {
        MAGAZINE_TOP_SUBDIV: "MAGAZINE_TOP_SUBDIV",
        MAIN_MAGAZINE: "MAIN_MAGAZINE",
    },
    EVENT: {
        EVENT_MAIN_01: "EVENT_MAIN_01",
        EVENT_MAIN_02: "EVENT_MAIN_02",
        EVENT_SEARCH_RECOMMEND: "EVENT_SEARCH_RECOMMEND",
        EVENT_SUB_MAIN: "EVENT_SUB_MAIN",
    },
    ADVERTISER: {
        ADVERTISER_MAIN_VILLAGE: "ADVERTISER_MAIN_VILLAGE",
        ADVERTISER_LIFE: "ADVERTISER_LIFE",
    },
};
