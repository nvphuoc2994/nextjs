export const getBase64 = async (file: Blob) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);
    });
};

const getCurrentURL = () => {
    const canonicalEl = document.querySelector("link[rel=canonical]");
    return canonicalEl ? canonicalEl.href : window.location.href;
};

export const shareNative = (
    config = { title: "", text: "", url: getCurrentURL() },
    onSuccess: ((value: void) => void | PromiseLike<void>) | null | undefined,
    onError: ((reason: any) => PromiseLike<never>) | null | undefined
) => {
    const url = config.url || getCurrentURL();
    const title = config.title || document.title;
    const text = config.text;
    //@ts-ignore
    if (window.ReactNativeWebView) {
        const payload = JSON.stringify({
            type: "share",
            data: { text, title, url },
        });
        //@ts-ignore
        window.ReactNativeWebView.postMessage(payload);
        return;
    }

    if (!navigator.share)
        return onError && onError("Browser can't support share!!!");
    navigator.share({ text, title, url }).then(onSuccess).catch(onError);
};

export const truncateSummary = (text = "", maxLength = 80) => {
    if (text && text.length > maxLength)
        return text.slice(0, maxLength - 3) + "...";
    else return text;
};

export const isBrowser = typeof window !== "undefined";
