import "../styles/globals.scss";
import type { AppProps } from "next/app";
import { RecoilRoot } from "recoil";
import "src/i18n";
import "isomorphic-fetch";

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <RecoilRoot>
            <Component {...pageProps} />
        </RecoilRoot>
    );
}
export default MyApp;
