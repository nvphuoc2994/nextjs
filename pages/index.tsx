import Layout from "@components/Layouts";
import HotArticle from "@components/HotArticle";
import RealtimeNews from "@components/RealtimeNews";
import Summary from "@components/Summary";
import MainNews from "@components/MainNews";

export default function Home() {
    return (
        <Layout title="Hello VN">
            <div className="divide-y divide-greyLight2 divide-solid space-y-4 mt-9">
                <HotArticle />
                <Summary />
                <RealtimeNews />
                <MainNews />
            </div>
        </Layout>
    );
}
